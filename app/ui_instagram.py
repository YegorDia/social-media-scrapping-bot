import threading
import datetime
import random
import time
from . import wx, utils, Instagram, instagram_api, log

# Bot parameters
INSTAGRAM_IDS = []
INSTAGRAM_MESSAGES = []
INSTAGRAM_LIKE_PER_DAY = 10
INSTAGRAM_LIKE_PER_ID = 5
INSTAGRAM_LIKE_TODAY = 0
INSTAGRAM_TIME_INTERVAL_MAX = 30
INSTAGRAM_TIME = datetime.datetime.now()
INSTAGRAM_BOT_IS_RUNNING = False
INSTAGRAM_THREAD_RUNNING = False

# API parameters
INSTAGRAM_LAST_REQUEST_TIME = None
INSTAGRAM_LAST_REQUESTS = 0
INSTAGRAM_LAST_REQUESTS_MAX = 20
INSTAGRAM_LAST_REQUESTS_MAX_INTERVAL = 60*15


class InstagramPage(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.processing = False
        self.bot = None
        self.browser = None
        self.processing_label = wx.StaticText(self, label="Processing...", pos=(200, 130))
        self.processing_label.Hide()

        # Init objects
        self.controls = {
            'auth_button': wx.Button(self, label="Authorize", id=wx.ID_OK, pos=(10, 10), size=(555, 290)),

            'get_following_title': wx.StaticText(self, label="SCRAPPING OPTIONS", pos=(20, 30)),
            'get_following_label': wx.StaticText(self, label="Enter Username:", pos=(20, 70)),
            'get_following_field': wx.TextCtrl(self, value="", size=(300, 20), pos=(110, 68)),
            'get_following_button': wx.Button(self, label="Get followers", id=wx.ID_OK, pos=(420, 65)),

            'bot_title': wx.StaticText(self, label="BOT OPTIONS", pos=(20, 150)),
            'open_following_label': wx.StaticText(self, label="Followers List", pos=(20, 190)),
            'open_following_button': wx.Button(self, label="Open followers .csv", id=wx.ID_OK, pos=(145, 185)),
            'open_following_list': wx.ListCtrl(self, wx.NewId(), style=wx.LC_REPORT | wx.SUNKEN_BORDER, size=(250, 300), pos=(20, 220)),
            'open_direct_button': wx.Button(self, label="Open comments .csv", id=wx.ID_OK, pos=(145, 530)),
            'send_direct_button': wx.Button(self, label="Send comments", id=wx.ID_OK, pos=(145, 560)),

            'bot_stat_title': wx.StaticText(self, label="BOT STATISTICS", pos=(290, 150)),
        }
        self.stat_controls = {}
        self.title_font = wx.Font(14, wx.DEFAULT, wx.NORMAL, wx.BOLD)
        self.controls['get_following_title'].SetFont(self.title_font)
        self.controls['bot_title'].SetFont(self.title_font)
        self.controls['bot_stat_title'].SetFont(self.title_font)

        self.stats = {
            'Likes Per Day': '%s/%s' % (str(INSTAGRAM_LIKE_TODAY), str(INSTAGRAM_LIKE_PER_DAY)),
            'Likes Per User': '%s' % (str(INSTAGRAM_LIKE_PER_ID)),
            'Max Time Interval': '%s seconds' % str(INSTAGRAM_TIME_INTERVAL_MAX),
            'Requests Sent': '%s/%s' % (str(INSTAGRAM_LAST_REQUESTS), str(INSTAGRAM_LAST_REQUESTS_MAX)),
            'Requests Limit': '%s/%s minutes' % (str(INSTAGRAM_LAST_REQUESTS_MAX), str(INSTAGRAM_LAST_REQUESTS_MAX_INTERVAL/60)),
            'Last Request Time': '-',
            'Current Time': INSTAGRAM_TIME.strftime("%b %d %Y %H:%M:%S")
        }

        stats_pos = {'x': 290, 'y': 190}
        tilt = 0
        for key, value in self.stats.iteritems():
            self.controls[key] = wx.StaticText(self, label=key, pos=(stats_pos['x'], stats_pos['y'] + tilt))
            self.controls[key + '_val()'] = wx.StaticText(self, label=value, pos=(stats_pos['x'] + 155, stats_pos['y'] + tilt))
            tilt += 20

        self.controls['bot_settings_button'] = wx.Button(self, label="Settings", id=wx.ID_OK, pos=(stats_pos['x'] + 155, stats_pos['y'] + tilt + 40))
        self.controls['bot_run_button'] = wx.Button(self, label="Run Bot", id=wx.ID_OK, pos=(stats_pos['x'] + 155, stats_pos['y'] + tilt + 70))
        self.controls['bot_status'] = wx.StaticText(self, label="Status: -", pos=(stats_pos['x'], stats_pos['y'] + tilt + 110))

        # Binds
        self.Bind(EVT_INSTAGRAM_FOLLOWING, self.on_following_harvested)

        self.controls['auth_button'].Bind(wx.EVT_BUTTON, self.on_auth, id=wx.ID_OK)
        self.controls['get_following_button'].Bind(wx.EVT_BUTTON, self.on_get_following, id=wx.ID_OK)
        self.controls['open_following_button'].Bind(wx.EVT_BUTTON, self.open_following_csv, id=wx.ID_OK)
        self.controls['bot_settings_button'].Bind(wx.EVT_BUTTON, self.on_settings, id=wx.ID_OK)
        self.controls['bot_run_button'].Bind(wx.EVT_BUTTON, self.on_run_bot, id=wx.ID_OK)
        self.controls['open_direct_button'].Bind(wx.EVT_BUTTON, self.open_messages_csv, id=wx.ID_OK)
        self.controls['send_direct_button'].Bind(wx.EVT_BUTTON, self.send_messages, id=wx.ID_OK)

        for key, value in self.controls.iteritems():
                value.Hide()
        self.controls['auth_button'].Show()

    def show_message(self, message):
        wx.MessageBox(message, 'Info', wx.OK | wx.ICON_INFORMATION)

    def update_statistics(self, status=None):
        last_request_time = INSTAGRAM_LAST_REQUEST_TIME
        if last_request_time is None:
            last_request_time = '-'
        else:
            last_request_time = last_request_time.strftime("%b %d %Y %H:%M:%S")

        stats = {
            'Likes Per Day': '%s/%s' % (str(INSTAGRAM_LIKE_TODAY), str(INSTAGRAM_LIKE_PER_DAY)),
            'Likes Per User': '%s' % (str(INSTAGRAM_LIKE_PER_ID)),
            'Max Time Interval': '%s seconds' % str(INSTAGRAM_TIME_INTERVAL_MAX),
            'Requests Sent': '%s/%s' % (str(INSTAGRAM_LAST_REQUESTS), str(INSTAGRAM_LAST_REQUESTS_MAX)),
            'Requests Limit': '%s/%s minutes' % (str(INSTAGRAM_LAST_REQUESTS_MAX), str(INSTAGRAM_LAST_REQUESTS_MAX_INTERVAL/60)),
            'Last Request Time': last_request_time,
            'Current Time': INSTAGRAM_TIME.strftime("%b %d %Y %H:%M:%S")
        }

        for key, value in stats.iteritems():
            self.controls[key].SetLabel(key)
            self.controls[key + '_val()'].SetLabel(value)

        if status:
            self.controls['bot_status'].SetLabel('Status: %s' % status)

    def toggle_processing(self, reason=None):
        if self.processing is False:
            self.processing_label.SetLabel("Processing: %s" % str(reason))
            self.processing_label.Show()
            self.processing = True

            for key, value in self.controls.iteritems():
                value.Hide()

            return
        try:
            self.processing_label.Hide()
        except:
            pass
        self.processing = False

    def toggle_main(self):
        if self.processing is True:
            self.toggle_processing()
        main_elements = [
            'get_following_title',
            'get_following_label',
            'get_following_field',
            'get_following_button',

            'bot_title',
            'open_following_label',
            'open_following_button',
            'open_following_list',

            'bot_stat_title',
            'bot_settings_button',
            'bot_run_button',
            'bot_status',
            'open_direct_button',
            'send_direct_button'
        ]
        for key in self.stats.keys():
            main_elements.append(key)
            main_elements.append(key + '_val()')

        for value in main_elements:
            self.controls[value].Show()

    def on_auth(self, event):
        self.toggle_processing(reason='Authorization')

        dialog = InstagramAuthWebView(self, None, -1)
        url = Instagram.get_auth_url()
        dialog.browser.LoadURL(url)
        self.browser = dialog.browser
        dialog.Show()

    def on_settings(self, event):
        dialog = InstagramSettings(self, 'Instagram Bot Settings')
        dialog.Show()

    def on_get_following(self, event):
        if not self.processing:
            username = self.controls['get_following_field'].GetValue()
            if len(username) > 0:
                self.toggle_processing('Scrapping Followers of "%s"...' % username)
                worker = FollowingThread(self, None, username)
                worker.start()
                return
            self.show_message('Error! Type username or search word')

    def on_following_harvested(self, event):
        if event.GetFollowing():
            self.toggle_main()
            self.show_message('Done!')

    def open_following_csv(self, event):
        openFileDialog = wx.FileDialog(self, "Open CSV file", "", "",
                                               "CSV files (*.csv)|*.csv", wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)

        if openFileDialog.ShowModal() == wx.ID_CANCEL:
            return

        ids = []
        try:
            ids = utils.get_csv_content(openFileDialog.GetPath())
        except Exception, e:
            self.show_message('Error! %s' % e.message)

        if len(ids) > 0:
            global INSTAGRAM_IDS
            INSTAGRAM_IDS = ids

            self.controls['open_following_list'].ClearAll()
            self.controls['open_following_list'].InsertColumn(0, '#')
            self.controls['open_following_list'].InsertColumn(1, 'Instagram ID')
            self.controls['open_following_list'].InsertColumn(2, 'Direct Message')

            for index, value in enumerate(INSTAGRAM_IDS):
                pos = self.controls['open_following_list'].InsertStringItem(0, str(index + 1))
                self.controls['open_following_list'].SetStringItem(pos, 1, value)

            self.show_message('Done! Instagram ids loaded')
            return
        self.show_message('Error! %s' % 'No Instagram ids')

    def on_run_bot(self, event):
        if len(INSTAGRAM_IDS) > 0:
            global INSTAGRAM_BOT_IS_RUNNING
            if INSTAGRAM_BOT_IS_RUNNING is True:
                INSTAGRAM_BOT_IS_RUNNING = False
                self.controls['bot_run_button'].SetLabel('Run Bot')
            else:

                if INSTAGRAM_THREAD_RUNNING is False:
                    INSTAGRAM_BOT_IS_RUNNING = True
                    global INSTAGRAM_LIKE_TODAY
                    global INSTAGRAM_LAST_REQUESTS
                    INSTAGRAM_LIKE_TODAY = 0
                    INSTAGRAM_LAST_REQUESTS = 0
                    self.bot = InstagramBotThread(self)
                    self.bot.start()
                    self.controls['bot_run_button'].SetLabel('Stop Bot')
                    self.update_statistics()
                else:
                    self.show_message('Error! Please, wait until bot thread will end its work.')
            return
        self.show_message('Error! Upload ids to run the bot.')

    def open_messages_csv(self, event):
        if len(INSTAGRAM_IDS) > 0:
            openFileDialog = wx.FileDialog(self, "Open CSV file", "", "",
                                                   "CSV files (*.csv)|*.csv", wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)

            if openFileDialog.ShowModal() == wx.ID_CANCEL:
                return

            messages = []
            try:
                messages = utils.get_csv_content(openFileDialog.GetPath())
            except Exception, e:
                self.show_message('Error! %s' % e.message)

            if len(messages) > 0:
                global INSTAGRAM_MESSAGES
                INSTAGRAM_MESSAGES = messages

                message_index = 0
                for index, value in enumerate(INSTAGRAM_IDS):
                    if message_index >= len(INSTAGRAM_MESSAGES):
                        message_index = 0

                    try:
                        pos_index = len(INSTAGRAM_IDS) - (index+1)
                        self.controls['open_following_list'].SetStringItem(pos_index, 2, INSTAGRAM_MESSAGES[message_index])
                    except:
                        break

                    message_index += 1

                self.show_message('Done! Instagram messages loaded')
                return
            self.show_message('Error! %s' % 'No Instagram messages')
        self.show_message('Error! %s' % 'No Instagram Ids')

    def send_messages(self, event):
        if len(INSTAGRAM_IDS) > 0 and len(INSTAGRAM_MESSAGES) > 0:
            messaging_worker = InstagramMessagingThread(self)
            messaging_worker.start()
            return


class InstagramAuthWebView(wx.Dialog):
    def __init__(self, parent=None, *args, **kwds):
        wx.Dialog.__init__(self, *args, **kwds)
        self.parent = parent
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.browser = wx.html2.WebView.New(self)
        sizer.Add(self.browser, 1, wx.EXPAND, 10)
        self.SetSizer(sizer)
        self.SetSize((700, 700))
        self.Bind(wx.html2.EVT_WEBVIEW_NAVIGATED, self.on_navigated, self.browser)
        # self.Bind(EVT_INSTAGRAM_AUTH, self.on_authed)
        global Instagram
        self.Bind(wx.html2.EVT_WEBVIEW_LOADED, Instagram.loaded, self.browser)

    def on_navigated(self, event):
        if Instagram.access_token is None:
            targetUrl = event.GetURL()
            worker = AuthThread(self, None, targetUrl)
            worker.start()


myEVT_INSTAGRAM_AUTH = wx.NewEventType()
EVT_INSTAGRAM_AUTH = wx.PyEventBinder(myEVT_INSTAGRAM_AUTH, 1)


class AuthEvent(wx.PyCommandEvent):
    def __init__(self, etype, eid, value=None):
        wx.PyCommandEvent.__init__(self, etype, eid)
        self._value = value

    def GetValue(self):
        return self._value


myEVT_INSTAGRAM_FOLLOWING = wx.NewEventType()
EVT_INSTAGRAM_FOLLOWING = wx.PyEventBinder(myEVT_INSTAGRAM_FOLLOWING, 1)


class FollowingEvent(wx.PyCommandEvent):
    def __init__(self, etype, eid, following=None):
        wx.PyCommandEvent.__init__(self, etype, eid)
        self._following = following

    def GetFollowing(self):
        return self._following


class FollowingThread(threading.Thread):
    def __init__(self, parent, following, username):
        threading.Thread.__init__(self)
        self._parent = parent
        self.username = username
        self._following = following

    def run(self):
        if self.username:
            try:
                following = Instagram.get_following(self.username)
            except Exception, e:
                self._parent.show_message('Error! %s' % e.message)
                self._parent.toggle_main()
                return

            names = [x['id'] for x in following]
            self._following = names
            utils.save_csv('instagram_%s' % self.username, names)


        evt = FollowingEvent(myEVT_INSTAGRAM_FOLLOWING, -1, self._following)
        wx.PostEvent(self._parent, evt)


class AuthThread(threading.Thread):
    def __init__(self, parent, value, url):
        threading.Thread.__init__(self)
        self._parent = parent
        self.url = url
        self._value = value

    def run(self):
        if self.url.find('code=') != -1 and Instagram.access_token is None:
            code = self.url.split('code=')[1]
            self._value = Instagram.get_access_token(code)
            self._parent.parent.toggle_main()
            # self._parent.Hide()


        # evt = AuthEvent(myEVT_INSTAGRAM_AUTH, -1, self._value)
        # wx.PostEvent(self._parent, evt)
            
            
class InstagramBotThread(threading.Thread):
    def __init__(self, parent):
        threading.Thread.__init__(self)
        self._parent = parent
        self.waiting_next_day = False
        self.start_time = datetime.datetime.now()
        self._stop = threading.Event()

    def stop(self):
        global INSTAGRAM_THREAD_RUNNING
        INSTAGRAM_THREAD_RUNNING = False
        self.update('Bot stopped')
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()

    def get_most_recent_posts(self, id):
        global INSTAGRAM_LAST_REQUEST_TIME
        INSTAGRAM_LAST_REQUEST_TIME = datetime.datetime.now()
        self.update('Recent posts of user: %s' % str(id))
        return Instagram.get_posts(id, INSTAGRAM_LIKE_PER_DAY)

    def next_day_came(self):
        global INSTAGRAM_LIKE_TODAY
        global INSTAGRAM_LAST_REQUESTS
        INSTAGRAM_LIKE_TODAY = 0
        INSTAGRAM_LAST_REQUESTS = 0
        self.waiting_next_day = False

    def like_media(self, link):
        global INSTAGRAM_LAST_REQUEST_TIME
        global INSTAGRAM_LIKE_TODAY
        INSTAGRAM_LAST_REQUEST_TIME = datetime.datetime.now()
        try:
            self.update('Like for post: %s' % link)
            Instagram.post_like(link, self._parent.browser)
            INSTAGRAM_LIKE_TODAY += 1
        except:
            pass

        if INSTAGRAM_LIKE_TODAY >= INSTAGRAM_LIKE_PER_DAY:
            self.update('Waiting next day, reached like per day')
            self.waiting_next_day = True

    def count_request(self):
        global INSTAGRAM_LAST_REQUESTS
        if INSTAGRAM_LAST_REQUESTS + 1 <= INSTAGRAM_LAST_REQUESTS_MAX:
            INSTAGRAM_LAST_REQUESTS += 1
            return True

        end_time = datetime.datetime.now()
        difference = (end_time - self.start_time).seconds
        to_wait = INSTAGRAM_LAST_REQUESTS_MAX_INTERVAL - difference
        self.update('Waiting Rate Limit Interval %s minutes' % (str(to_wait/60)))
        time.sleep(to_wait)

        self.start_time = datetime.datetime.now()
        INSTAGRAM_LAST_REQUESTS = 0
        return True

    def update(self, status=None):
        self._parent.update_statistics(status)

    def run(self):
        self.start_time = datetime.datetime.now()

        global INSTAGRAM_THREAD_RUNNING
        INSTAGRAM_THREAD_RUNNING = True

        index = 0
        global INSTAGRAM_BOT_IS_RUNNING
        while INSTAGRAM_BOT_IS_RUNNING is True:


            random_secs = random.randrange(1, INSTAGRAM_TIME_INTERVAL_MAX + 1, 2)
            self.update('Waiting Random Time [Random: %s secs]' % str(random_secs))
            time.sleep(random_secs)

            if index >= len(INSTAGRAM_IDS):
                index = 0

            posts = []
            if self.waiting_next_day is False:
                if self.count_request():
                    posts = self.get_most_recent_posts(INSTAGRAM_IDS[index])

                for post in posts:
                    self.like_media(post['link'])
                    random_secs = random.randrange(1, INSTAGRAM_TIME_INTERVAL_MAX + 1, 2)
                    self.update('Waiting Random Time [Random: %s secs]' % str(random_secs))
                    time.sleep(random_secs)

                index += 1

            if INSTAGRAM_LIKE_TODAY >= INSTAGRAM_LIKE_PER_DAY:
                self.update('Waiting next day, reached likes per day')
                self.waiting_next_day = True

            global INSTAGRAM_TIME
            day = datetime.datetime.now().day
            if day != INSTAGRAM_TIME.day:
                self.next_day_came()

            INSTAGRAM_TIME = datetime.datetime.now()

        INSTAGRAM_THREAD_RUNNING = False
        self.update('Bot stopped')
        return


class InstagramMessagingThread(threading.Thread):
    def __init__(self, parent):
        threading.Thread.__init__(self)
        self._parent = parent
        self._stop = threading.Event()

    def stop(self):
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()

    def send_message(self, id, message):
        posts = Instagram.get_posts(id)
        Instagram.post_comment(posts[0]['link'], message, self._parent.browser)

    def run(self):
        index = 0
        sent = 0
        errors = []

        for user_id in INSTAGRAM_IDS:
            if index >= len(INSTAGRAM_MESSAGES):
                index = 0
            try:
                self.send_message(user_id, INSTAGRAM_MESSAGES[index])
                time.sleep(5)
                sent += 1
            except Exception, e:
                errors.append('Sending to %s, API message: %s ' % (str(user_id), str(e.message)))
                pass

            index += 1

        self._parent.show_message('Successfuly sent %s / %s comments.' % (str(sent), str(len(INSTAGRAM_IDS))))
        if len(errors) > 0:
            self._parent.show_message('Errors: \n\n %s' % '\n'.join(errors))
        return


class InstagramSettings(wx.Frame):
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, title=title, size=(330, 200))
        self.parent = parent
        tilt = 0

        self.INSTAGRAM_LIKE_PER_ID_label = wx.StaticText(self, label="Likes Per User:", pos=(10, 10 + tilt))
        self.INSTAGRAM_LIKE_PER_ID_field = wx.TextCtrl(self, value=str(INSTAGRAM_LIKE_PER_ID), size=(50, 20), pos=(250, 10 + tilt))
        tilt += 30

        self.INSTAGRAM_LIKE_PER_DAY_label = wx.StaticText(self, label="Likes Per Day:", pos=(10, 10 + tilt))
        self.INSTAGRAM_LIKE_PER_DAY_field = wx.TextCtrl(self, value=str(INSTAGRAM_LIKE_PER_DAY), size=(50, 20), pos=(250, 10 + tilt))
        tilt += 30

        self.INSTAGRAM_TIME_INTERVAL_MAX_label = wx.StaticText(self, label="Random Max Time Interval (secs):", pos=(10, 10 + tilt))
        self.INSTAGRAM_TIME_INTERVAL_MAX_field = wx.TextCtrl(self, value=str(INSTAGRAM_TIME_INTERVAL_MAX), size=(50, 20), pos=(250, 10 + tilt))

        self.save_button = wx.Button(self, label="Save Settings", id=wx.ID_OK, pos=(200, 50 + tilt), size=(100, 30))
        self.save_button.Bind(wx.EVT_BUTTON, self.on_save, id=wx.ID_OK)

        self.Bind(wx.EVT_CLOSE, self.OnExit)

    def on_save(self, event):
        try:
            global INSTAGRAM_LIKE_PER_DAY
            INSTAGRAM_LIKE_PER_DAY = int(self.INSTAGRAM_LIKE_PER_DAY_field.GetValue())

            global INSTAGRAM_LIKE_PER_ID
            INSTAGRAM_LIKE_PER_ID = int(self.INSTAGRAM_LIKE_PER_ID_field.GetValue())

            global INSTAGRAM_TIME_INTERVAL_MAX
            INSTAGRAM_TIME_INTERVAL_MAX = int(self.INSTAGRAM_TIME_INTERVAL_MAX_field.GetValue())
        except Exception, e:
            self.parent.show_message('Error! %s' % e.message)
        self.parent.update_statistics()
        self.Destroy()

    def OnExit(self, e):
        self.Destroy()