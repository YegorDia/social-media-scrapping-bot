import threading
import datetime
import random
import time
from . import wx, utils, Twitter, twitter_api, log

# Bot parameters
TWITTER_IDS = []
TWITTER_MESSAGES = []
TWITTER_FAV_PER_DAY = 10
TWITTER_FAV_PER_ID = 5
TWITTER_FAV_TODAY = 0
TWITTER_TIME_INTERVAL_MAX = 30
TWITTER_TIME = datetime.datetime.now()
TWITTER_BOT_IS_RUNNING = False
TWITTER_THREAD_RUNNING = False

# API parameters
TWITTER_LAST_REQUEST_TIME = None
TWITTER_LAST_REQUESTS = 0
TWITTER_LAST_REQUESTS_MAX = 15
TWITTER_LAST_REQUESTS_MAX_INTERVAL = 60*15


class TwitterPage(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.processing = False
        self.bot = None
        self.processing_label = wx.StaticText(self, label="Processing...", pos=(200, 130))
        self.processing_label.Hide()

        # Init objects
        self.controls = {
            'auth_button': wx.Button(self, label="Authorize", id=wx.ID_OK, pos=(10, 10), size=(555, 290)),

            'get_following_title': wx.StaticText(self, label="SCRAPPING OPTIONS", pos=(20, 30)),
            'get_following_label': wx.StaticText(self, label="Enter Username:", pos=(20, 70)),
            'get_following_field': wx.TextCtrl(self, value="", size=(300, 20), pos=(110, 68)),
            'get_following_button': wx.Button(self, label="Get followers", id=wx.ID_OK, pos=(420, 65)),

            'bot_title': wx.StaticText(self, label="BOT OPTIONS", pos=(20, 150)),
            'open_following_label': wx.StaticText(self, label="Followers List", pos=(20, 190)),
            'open_following_button': wx.Button(self, label="Open followers .csv", id=wx.ID_OK, pos=(145, 185)),
            'open_following_list': wx.ListCtrl(self, wx.NewId(), style=wx.LC_REPORT | wx.SUNKEN_BORDER, size=(250, 300), pos=(20, 220)),
            'open_direct_button': wx.Button(self, label="Open messages .csv", id=wx.ID_OK, pos=(145, 530)),
            'send_direct_button': wx.Button(self, label="Send messages", id=wx.ID_OK, pos=(145, 560)),

            'bot_stat_title': wx.StaticText(self, label="BOT STATISTICS", pos=(290, 150)),
        }
        self.stat_controls = {}
        self.title_font = wx.Font(14, wx.DEFAULT, wx.NORMAL, wx.BOLD)
        self.controls['get_following_title'].SetFont(self.title_font)
        self.controls['bot_title'].SetFont(self.title_font)
        self.controls['bot_stat_title'].SetFont(self.title_font)

        self.stats = {
            'Favs Per Day': '%s/%s' % (str(TWITTER_FAV_TODAY), str(TWITTER_FAV_PER_DAY)),
            'Favs Per User': '%s' % str(TWITTER_FAV_PER_ID),
            'Max Time Interval': '%s seconds' % str(TWITTER_TIME_INTERVAL_MAX),
            'Requests Sent': '%s/%s' % (str(TWITTER_LAST_REQUESTS), str(TWITTER_LAST_REQUESTS_MAX)),
            'Requests Limit': '%s/%s minutes' % (str(TWITTER_LAST_REQUESTS_MAX), str(TWITTER_LAST_REQUESTS_MAX_INTERVAL/60)),
            'Last Request Time': '-',
            'Current Time': TWITTER_TIME.strftime("%b %d %Y %H:%M:%S")
        }

        stats_pos = {'x': 290, 'y': 190}
        tilt = 0
        for key, value in self.stats.iteritems():
            self.controls[key] = wx.StaticText(self, label=key, pos=(stats_pos['x'], stats_pos['y'] + tilt))
            self.controls[key + '_val()'] = wx.StaticText(self, label=value, pos=(stats_pos['x'] + 155, stats_pos['y'] + tilt))
            tilt += 20

        self.controls['bot_settings_button'] = wx.Button(self, label="Settings", id=wx.ID_OK, pos=(stats_pos['x'] + 155, stats_pos['y'] + tilt + 40))
        self.controls['bot_run_button'] = wx.Button(self, label="Run Bot", id=wx.ID_OK, pos=(stats_pos['x'] + 155, stats_pos['y'] + tilt + 70))
        self.controls['bot_status'] = wx.StaticText(self, label="Status: -", pos=(stats_pos['x'], stats_pos['y'] + tilt + 110))

        # Binds
        self.Bind(EVT_TWITTER_FOLLOWING, self.on_following_harvested)

        self.controls['auth_button'].Bind(wx.EVT_BUTTON, self.on_auth, id=wx.ID_OK)
        self.controls['get_following_button'].Bind(wx.EVT_BUTTON, self.on_get_following, id=wx.ID_OK)
        self.controls['open_following_button'].Bind(wx.EVT_BUTTON, self.open_following_csv, id=wx.ID_OK)
        self.controls['bot_settings_button'].Bind(wx.EVT_BUTTON, self.on_settings, id=wx.ID_OK)
        self.controls['bot_run_button'].Bind(wx.EVT_BUTTON, self.on_run_bot, id=wx.ID_OK)
        self.controls['open_direct_button'].Bind(wx.EVT_BUTTON, self.open_messages_csv, id=wx.ID_OK)
        self.controls['send_direct_button'].Bind(wx.EVT_BUTTON, self.send_messages, id=wx.ID_OK)

        for key, value in self.controls.iteritems():
                value.Hide()
        self.controls['auth_button'].Show()

    def show_message(self, message):
        wx.MessageBox(message, 'Info', wx.OK | wx.ICON_INFORMATION)

    def update_statistics(self, status=None):
        last_request_time = TWITTER_LAST_REQUEST_TIME
        if last_request_time is None:
            last_request_time = '-'
        else:
            last_request_time = last_request_time.strftime("%b %d %Y %H:%M:%S")

        stats = {
            'Favs Per Day': '%s/%s' % (str(TWITTER_FAV_TODAY), str(TWITTER_FAV_PER_DAY)),
            'Favs Per User': '%s' % str(TWITTER_FAV_PER_ID),
            'Max Time Interval': '%s seconds' % str(TWITTER_TIME_INTERVAL_MAX),
            'Requests Sent': '%s/%s' % (str(TWITTER_LAST_REQUESTS), str(TWITTER_LAST_REQUESTS_MAX)),
            'Requests Limit': '%s/%s minutes' % (str(TWITTER_LAST_REQUESTS_MAX), str(TWITTER_LAST_REQUESTS_MAX_INTERVAL/60)),
            'Last Request Time': last_request_time,
            'Current Time': TWITTER_TIME.strftime("%b %d %Y %H:%M:%S")
        }

        for key, value in stats.iteritems():
            self.controls[key].SetLabel(key)
            self.controls[key + '_val()'].SetLabel(value)

        if status:
            self.controls['bot_status'].SetLabel('Status: %s' % status)

    def toggle_processing(self, reason=None):
        if self.processing is False:
            self.processing_label.SetLabel("Processing: %s" % str(reason))
            self.processing_label.Show()
            self.processing = True

            for key, value in self.controls.iteritems():
                value.Hide()

            return
        try:
            self.processing_label.Hide()
        except:
            pass
        self.processing = False

    def toggle_main(self):
        if self.processing is True:
            self.toggle_processing()
        main_elements = [
            'get_following_title',
            'get_following_label',
            'get_following_field',
            'get_following_button',

            'bot_title',
            'open_following_label',
            'open_following_button',
            'open_following_list',

            'bot_stat_title',
            'bot_settings_button',
            'bot_run_button',
            'bot_status',
            'open_direct_button',
            'send_direct_button'
        ]
        for key in self.stats.keys():
            main_elements.append(key)
            main_elements.append(key + '_val()')

        for value in main_elements:
            self.controls[value].Show()

    def on_auth(self, event):
        self.toggle_processing(reason='Authorization')

        dialog = TwitterAuthWebView(self, None, -1)
        url = Twitter.get_auth_url()
        dialog.browser.LoadURL(url)
        dialog.Show()

    def on_settings(self, event):
        dialog = TwitterSettings(self, 'Twitter Bot Settings')
        dialog.Show()

    def on_get_following(self, event):
        if not self.processing:
            username = self.controls['get_following_field'].GetValue()
            if len(username) > 0:
                self.toggle_processing('Scrapping Following of "%s"...' % username)
                worker = FollowingThread(self, None, username)
                worker.start()
                return
            self.show_message('Error! Enter username to fetch followers')

    def on_following_harvested(self, event):
        if event.GetFollowing():
            self.toggle_main()
            self.show_message('Done!')

    def open_following_csv(self, event):
        openFileDialog = wx.FileDialog(self, "Open CSV file", "", "",
                                               "CSV files (*.csv)|*.csv", wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)

        if openFileDialog.ShowModal() == wx.ID_CANCEL:
            return

        ids = []
        try:
            ids = utils.get_csv_content(openFileDialog.GetPath())
        except Exception, e:
            self.show_message('Error! %s' % e.message)

        if len(ids) > 0:
            global TWITTER_IDS
            TWITTER_IDS = ids

            self.controls['open_following_list'].ClearAll()
            self.controls['open_following_list'].InsertColumn(0, '#')
            self.controls['open_following_list'].InsertColumn(1, 'Twitter ID')
            self.controls['open_following_list'].InsertColumn(2, 'Direct Message')

            for index, value in enumerate(TWITTER_IDS):
                pos = self.controls['open_following_list'].InsertStringItem(0, str(index + 1))
                self.controls['open_following_list'].SetStringItem(pos, 1, value)

            self.show_message('Done! Twitter ids loaded')
            return
        self.show_message('Error! %s' % 'No Twitter ids')

    def on_run_bot(self, event):
        if len(TWITTER_IDS) > 0:
            global TWITTER_BOT_IS_RUNNING
            if TWITTER_BOT_IS_RUNNING is True:
                TWITTER_BOT_IS_RUNNING = False
                self.controls['bot_run_button'].SetLabel('Run Bot')
            else:

                if TWITTER_THREAD_RUNNING is False:
                    TWITTER_BOT_IS_RUNNING = True
                    global TWITTER_FAV_TODAY
                    global TWITTER_LAST_REQUESTS
                    TWITTER_FAV_TODAY = 0
                    TWITTER_LAST_REQUESTS = 0
                    self.bot = TwitterBotThread(self)
                    self.bot.start()
                    self.controls['bot_run_button'].SetLabel('Stop Bot')
                    self.update_statistics()
                else:
                    self.show_message('Error! Please, wait until bot thread will end its work.')
            return
        self.show_message('Error! Upload ids to run the bot.')

    def open_messages_csv(self, event):
        if len(TWITTER_IDS) > 0:
            openFileDialog = wx.FileDialog(self, "Open CSV file", "", "",
                                                   "CSV files (*.csv)|*.csv", wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)

            if openFileDialog.ShowModal() == wx.ID_CANCEL:
                return

            messages = []
            try:
                messages = utils.get_csv_content(openFileDialog.GetPath())
            except Exception, e:
                self.show_message('Error! %s' % e.message)

            if len(messages) > 0:
                global TWITTER_MESSAGES
                TWITTER_MESSAGES = messages

                message_index = 0
                for index, value in enumerate(TWITTER_IDS):
                    if message_index >= len(TWITTER_MESSAGES):
                        message_index = 0

                    try:
                        pos_index = len(TWITTER_IDS) - (index+1)
                        self.controls['open_following_list'].SetStringItem(pos_index, 2, TWITTER_MESSAGES[message_index])
                    except:
                        break

                    message_index += 1

                self.show_message('Done! Twitter messages loaded')
                return
            self.show_message('Error! %s' % 'No Twitter messages')
        self.show_message('Error! %s' % 'No Twitter Ids')

    def send_messages(self, event):
        if len(TWITTER_IDS) > 0 and len(TWITTER_MESSAGES) > 0:
            messaging_worker = TwitterMessagingThread(self)
            messaging_worker.start()
            return
        self.show_message('Error! %s' % 'No Twitter Ids and messages')


class TwitterAuthWebView(wx.Dialog):
    def __init__(self, parent=None, *args, **kwds):
        wx.Dialog.__init__(self, *args, **kwds)
        self.parent = parent
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.browser = wx.html2.WebView.New(self)
        sizer.Add(self.browser, 1, wx.EXPAND, 10)
        self.SetSizer(sizer)
        self.SetSize((700, 700))
        self.Bind(wx.html2.EVT_WEBVIEW_NAVIGATED, self.on_navigated, self.browser)
        # self.Bind(EVT_TWITTER_AUTH, self.on_authed)

    def on_navigated(self, event):
        if Twitter.access_token is None:
            targetUrl = event.GetURL()
            worker = AuthThread(self, None, None, targetUrl)
            worker.start()


myEVT_TWITTER_AUTH = wx.NewEventType()
EVT_TWITTER_AUTH = wx.PyEventBinder(myEVT_TWITTER_AUTH, 1)


class AuthEvent(wx.PyCommandEvent):
    def __init__(self, etype, eid, token=None, secret=None):
        wx.PyCommandEvent.__init__(self, etype, eid)
        self._token = token
        self._secret = secret

    def GetToken(self):
        return self._token

    def GetSecret(self):
        return self._secret

myEVT_TWITTER_FOLLOWING = wx.NewEventType()
EVT_TWITTER_FOLLOWING = wx.PyEventBinder(myEVT_TWITTER_FOLLOWING, 1)


class FollowingEvent(wx.PyCommandEvent):
    def __init__(self, etype, eid, following=None):
        wx.PyCommandEvent.__init__(self, etype, eid)
        self._following = following

    def GetFollowing(self):
        return self._following


class FollowingThread(threading.Thread):
    def __init__(self, parent, following, username):
        threading.Thread.__init__(self)
        self._parent = parent
        self.username = username
        self._following = following

    def run(self):
        if self.username:
            try:
                following = [x.screen_name for x in Twitter.get_following(self.username)]
            except Exception, e:
                self._parent.show_message('Error! %s' % e.message)
                self._parent.toggle_main()
                return

            self._following = following
            utils.save_csv('twitter_%s' % self.username, following)

        evt = FollowingEvent(myEVT_TWITTER_FOLLOWING, -1, self._following)
        wx.PostEvent(self._parent, evt)


class AuthThread(threading.Thread):
    def __init__(self, parent, token, secret, url):
        threading.Thread.__init__(self)
        self._parent = parent
        self.url = url
        self._token = token
        self._secret = secret

    def run(self):
        if self.url.find('oauth_verifier=') != -1 and Twitter.access_token is None:
            verifier = self.url.split('oauth_verifier=')[1]
            access_token = Twitter.get_access_token(verifier)
            self._token = access_token['access_token']
            self._secret = access_token['access_token_secret']
            self._parent.parent.toggle_main()
            self._parent.Destroy()

        # evt = AuthEvent(myEVT_TWITTER_AUTH, -1, self._token, self._secret)
        # wx.PostEvent(self._parent, evt)


class TwitterBotThread(threading.Thread):
    def __init__(self, parent):
        threading.Thread.__init__(self)
        self._parent = parent
        self.waiting_next_day = False
        self.start_time = datetime.datetime.now()
        self._stop = threading.Event()

    def stop(self):
        global TWITTER_THREAD_RUNNING
        TWITTER_THREAD_RUNNING = False
        self.update('Bot stopped')
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()

    def get_most_recent_tweets(self, screen_name):
        global TWITTER_LAST_REQUEST_TIME
        TWITTER_LAST_REQUEST_TIME = datetime.datetime.now()
        self.update('Recent tweets of user: %s' % str(screen_name))
        return Twitter.get_posts(screen_name, TWITTER_FAV_PER_ID)

    def next_day_came(self):
        global TWITTER_FAV_TODAY
        global TWITTER_LAST_REQUESTS
        TWITTER_FAV_TODAY = 0
        TWITTER_LAST_REQUESTS = 0
        self.waiting_next_day = False

    def make_requests(self, tweets):
        global TWITTER_LAST_REQUEST_TIME
        global TWITTER_FAV_TODAY
        for tweet in tweets:
            if self.count_request():
                TWITTER_LAST_REQUEST_TIME = datetime.datetime.now()
                try:
                    self.update('Fav for tweet id: %s' % str(tweet.id))
                    Twitter.post_like(tweet.id)
                    TWITTER_FAV_TODAY += 1
                except:
                    break

                if TWITTER_FAV_TODAY >= TWITTER_FAV_PER_DAY:
                    self.update('Waiting next day, reached fav per day')
                    self.waiting_next_day = True
                    break

    def count_request(self):
        global TWITTER_LAST_REQUESTS
        if TWITTER_LAST_REQUESTS + 1 <= TWITTER_LAST_REQUESTS_MAX:
            TWITTER_LAST_REQUESTS += 1
            return True

        end_time = datetime.datetime.now()
        difference = (end_time - self.start_time).seconds
        to_wait = TWITTER_LAST_REQUESTS_MAX_INTERVAL - difference
        self.update('Waiting Rate Limit Interval %s minutes' % (str(to_wait/60)))
        time.sleep(to_wait)
        self.start_time = datetime.datetime.now()
        TWITTER_LAST_REQUESTS = 0
        return True

    def update(self, status=None):
        self._parent.update_statistics(status)

    def run(self):
        self.start_time = datetime.datetime.now()

        global TWITTER_THREAD_RUNNING
        TWITTER_THREAD_RUNNING = True

        index = 0
        global TWITTER_BOT_IS_RUNNING
        while TWITTER_BOT_IS_RUNNING is True:
            random_secs = random.randrange(1, TWITTER_TIME_INTERVAL_MAX + 1, 2)
            self.update('Waiting Random Time [Random: %s secs]' % str(random_secs))
            time.sleep(random_secs)

            tweets = []

            if index >= len(TWITTER_IDS):
                index = 0

            if self.waiting_next_day is False:
                if self.count_request():
                    tweets = self.get_most_recent_tweets(TWITTER_IDS[index])

                self.make_requests(tweets)
                index += 1

            if TWITTER_FAV_TODAY >= TWITTER_FAV_PER_DAY:
                self.update('Waiting next day, reached fav per day')
                self.waiting_next_day = True

            global TWITTER_TIME
            day = datetime.datetime.now().day
            if day != TWITTER_TIME.day:
                self.next_day_came()

            TWITTER_TIME = datetime.datetime.now()

        TWITTER_THREAD_RUNNING = False
        self.update('Bot stopped')
        return


class TwitterMessagingThread(threading.Thread):
    def __init__(self, parent):
        threading.Thread.__init__(self)
        self._parent = parent
        self._stop = threading.Event()

    def stop(self):
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()

    def send_message(self, user, message):
        Twitter.send_direct_message(user, message)

    def run(self):
        index = 0
        sent = 0
        errors = []

        for user_id in TWITTER_IDS:
            if index >= len(TWITTER_MESSAGES):
                index = 0
            try:
                self.send_message(user_id, TWITTER_MESSAGES[index])
                time.sleep(5)
                sent += 1
            except Exception, e:
                errors.append('Sending to %s, API message: %s ' % (str(user_id), str(e.message)))
                pass

            index += 1

        self._parent.show_message('Successfuly sent %s / %s messages.' % (str(sent), str(len(TWITTER_IDS))))
        if len(errors) > 0:
            self._parent.show_message('Errors: \n\n %s' % '\n'.join(errors))

        return


class TwitterSettings(wx.Frame):
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, title=title, size=(330, 200))
        self.parent = parent
        tilt = 0

        self.TWITTER_FAV_PER_DAY_label = wx.StaticText(self, label="Favs Per Day:", pos=(10, 10 + tilt))
        self.TWITTER_FAV_PER_DAY_field = wx.TextCtrl(self, value=str(TWITTER_FAV_PER_DAY), size=(50, 20), pos=(250, 10 + tilt))
        tilt += 30

        self.TWITTER_FAV_PER_ID_label = wx.StaticText(self, label="Favs Per User:", pos=(10, 10 + tilt))
        self.TWITTER_FAV_PER_ID_field = wx.TextCtrl(self, value=str(TWITTER_FAV_PER_ID), size=(50, 20), pos=(250, 10 + tilt))
        tilt += 30

        self.TWITTER_TIME_INTERVAL_MAX_label = wx.StaticText(self, label="Random Max Time Interval (secs):", pos=(10, 10 + tilt))
        self.TWITTER_TIME_INTERVAL_MAX_field = wx.TextCtrl(self, value=str(TWITTER_TIME_INTERVAL_MAX), size=(50, 20), pos=(250, 10 + tilt))



        self.save_button = wx.Button(self, label="Save Settings", id=wx.ID_OK, pos=(200, 50 + tilt), size=(100, 30))
        self.save_button.Bind(wx.EVT_BUTTON, self.on_save, id=wx.ID_OK)

        self.Bind(wx.EVT_CLOSE, self.OnExit)

    def on_save(self, event):
        try:
            global TWITTER_FAV_PER_DAY
            TWITTER_FAV_PER_DAY = int(self.TWITTER_FAV_PER_DAY_field.GetValue())

            global TWITTER_TIME_INTERVAL_MAX
            TWITTER_TIME_INTERVAL_MAX = int(self.TWITTER_TIME_INTERVAL_MAX_field.GetValue())

            global TWITTER_FAV_PER_ID
            TWITTER_FAV_PER_ID = int(self.TWITTER_FAV_PER_ID_field.GetValue())
        except Exception, e:
            self.parent.show_message('Error! %s' % e.message)
        self.parent.update_statistics()
        self.Destroy()

    def OnExit(self, e):
        self.Destroy()