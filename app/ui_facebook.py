import threading
import datetime
import random
import time
import urlparse
from . import wx, utils, Facebook, facebook_api, log

# Bot parameters
FACEBOOK_IDS = []
FACEBOOK_MESSAGES = []
FACEBOOK_FRIEND_PER_DAY = 50
FACEBOOK_FRIEND_TODAY = 0
FACEBOOK_FRIEND_PER_ID = 0
FACEBOOK_TIME_INTERVAL_MAX = 10
FACEBOOK_TIME = datetime.datetime.now()
FACEBOOK_BOT_IS_RUNNING = False
FACEBOOK_THREAD_RUNNING = False

# API parameters
FACEBOOK_LAST_REQUEST_TIME = None
FACEBOOK_LAST_REQUESTS = 0
FACEBOOK_LAST_REQUESTS_MAX = 100
FACEBOOK_LAST_REQUESTS_MAX_INTERVAL = 1


class FacebookPage(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.processing = False
        self.bot = None
        self.processing_label = wx.StaticText(self, label="Processing...", pos=(30, 130))
        self.processing_label.Hide()
        self.browser = None
        self.following_process = False

        # Init objects
        self.controls = {
            'auth_button': wx.Button(self, label="Authorize", id=wx.ID_OK, pos=(10, 10), size=(555, 290)),

            'get_following_title': wx.StaticText(self, label="SCRAPPING OPTIONS", pos=(20, 30)),
            'get_following_label': wx.StaticText(self, label="Enter URL's Path:    https://www.facebook.com/", pos=(20, 70)),
            'get_following_field': wx.TextCtrl(self, value="", size=(200, 20), pos=(275, 68)),
            'get_following_button': wx.Button(self, label="Get ids", id=wx.ID_OK, pos=(480, 65)),

            'bot_title': wx.StaticText(self, label="BOT OPTIONS", pos=(20, 150)),
            'open_following_label': wx.StaticText(self, label="IDS List", pos=(20, 190)),
            'open_following_button': wx.Button(self, label="Open ids .csv", id=wx.ID_OK, pos=(145, 185)),
            'open_following_list': wx.ListCtrl(self, wx.NewId(), style=wx.LC_REPORT | wx.SUNKEN_BORDER, size=(250, 300), pos=(20, 220)),
            'open_direct_button': wx.Button(self, label="Open messages .csv", id=wx.ID_OK, pos=(145, 530)),
            'send_direct_button': wx.Button(self, label="Send messages", id=wx.ID_OK, pos=(145, 560)),

            'bot_stat_title': wx.StaticText(self, label="BOT STATISTICS", pos=(290, 150)),
        }
        self.stat_controls = {}
        self.title_font = wx.Font(14, wx.DEFAULT, wx.NORMAL, wx.BOLD)
        self.controls['get_following_title'].SetFont(self.title_font)
        self.controls['bot_title'].SetFont(self.title_font)
        self.controls['bot_stat_title'].SetFont(self.title_font)

        self.stats = {
            'Friends Per Day': '%s/%s' % (str(FACEBOOK_FRIEND_TODAY), str(FACEBOOK_FRIEND_PER_DAY)),
            # 'Friends Per User': '%s' % str(FACEBOOK_FRIEND_PER_ID),
            'Max Time Interval': '%s seconds' % str(FACEBOOK_TIME_INTERVAL_MAX),
            # 'Requests Sent': '%s/%s' % (str(FACEBOOK_LAST_REQUESTS), str(FACEBOOK_LAST_REQUESTS_MAX)),
            'Requests Limit': 'NO LIMITS',#'%s/%s minutes' % (str(FACEBOOK_LAST_REQUESTS_MAX), str(FACEBOOK_LAST_REQUESTS_MAX_INTERVAL/60)),
            'Last Request Time': '-',
            'Current Time': FACEBOOK_TIME.strftime("%b %d %Y %H:%M:%S")
        }

        stats_pos = {'x': 290, 'y': 190}
        tilt = 0
        for key, value in self.stats.iteritems():
            self.controls[key] = wx.StaticText(self, label=key, pos=(stats_pos['x'], stats_pos['y'] + tilt))
            self.controls[key + '_val()'] = wx.StaticText(self, label=value, pos=(stats_pos['x'] + 155, stats_pos['y'] + tilt))
            tilt += 20

        self.controls['bot_settings_button'] = wx.Button(self, label="Settings", id=wx.ID_OK, pos=(stats_pos['x'] + 155, stats_pos['y'] + tilt + 40))
        self.controls['bot_run_button'] = wx.Button(self, label="Run Bot", id=wx.ID_OK, pos=(stats_pos['x'] + 155, stats_pos['y'] + tilt + 70))
        self.controls['bot_status'] = wx.StaticText(self, label="Status: -", pos=(stats_pos['x'], stats_pos['y'] + tilt + 110))

        # Binds
        self.Bind(EVT_FACEBOOK_FOLLOWING, self.on_following_harvested)

        self.controls['auth_button'].Bind(wx.EVT_BUTTON, self.on_auth, id=wx.ID_OK)
        self.controls['get_following_button'].Bind(wx.EVT_BUTTON, self.on_get_following, id=wx.ID_OK)
        self.controls['open_following_button'].Bind(wx.EVT_BUTTON, self.open_following_csv, id=wx.ID_OK)
        self.controls['bot_settings_button'].Bind(wx.EVT_BUTTON, self.on_settings, id=wx.ID_OK)
        self.controls['bot_run_button'].Bind(wx.EVT_BUTTON, self.on_run_bot, id=wx.ID_OK)
        self.controls['open_direct_button'].Bind(wx.EVT_BUTTON, self.open_messages_csv, id=wx.ID_OK)
        self.controls['send_direct_button'].Bind(wx.EVT_BUTTON, self.send_messages, id=wx.ID_OK)

        for key, value in self.controls.iteritems():
                value.Hide()
        self.controls['auth_button'].Show()

    def show_message(self, message):
        wx.MessageBox(message, 'Info', wx.OK | wx.ICON_INFORMATION)

    def update_statistics(self, status=None):
        last_request_time = FACEBOOK_LAST_REQUEST_TIME
        if last_request_time is None:
            last_request_time = '-'
        else:
            last_request_time = last_request_time.strftime("%b %d %Y %H:%M:%S")

        stats = {
            'Friends Per Day': '%s/%s' % (str(FACEBOOK_FRIEND_TODAY), str(FACEBOOK_FRIEND_PER_DAY)),
            # 'Friends Per User': '%s' % str(FACEBOOK_FRIEND_PER_ID),
            'Max Time Interval': '%s seconds' % str(FACEBOOK_TIME_INTERVAL_MAX),
            # 'Requests Sent': '%s/%s' % (str(FACEBOOK_LAST_REQUESTS), str(FACEBOOK_LAST_REQUESTS_MAX)),
            'Requests Limit': 'NO LIMITS',#'%s/%s minutes' % (str(FACEBOOK_LAST_REQUESTS_MAX), str(FACEBOOK_LAST_REQUESTS_MAX_INTERVAL/60)),
            'Last Request Time': last_request_time,
            'Current Time': FACEBOOK_TIME.strftime("%b %d %Y %H:%M:%S")
        }

        for key, value in stats.iteritems():
            self.controls[key].SetLabel(key)
            self.controls[key + '_val()'].SetLabel(value)

        if status:
            self.controls['bot_status'].SetLabel('Status: %s' % status)

    def toggle_processing(self, reason=None):
        if self.processing is False:
            self.processing_label.SetLabel("Processing: %s" % str(reason))
            self.processing_label.Show()
            self.processing = True

            for key, value in self.controls.iteritems():
                value.Hide()

            return
        try:
            self.processing_label.Hide()
        except:
            pass
        self.processing = False

    def toggle_main(self):
        if self.processing is True:
            self.toggle_processing()
        main_elements = [
            'get_following_title',
            'get_following_label',
            'get_following_field',
            'get_following_button',

            'bot_title',
            'open_following_label',
            'open_following_button',
            'open_following_list',

            'bot_stat_title',
            'bot_settings_button',
            'bot_run_button',
            'bot_status',
            'open_direct_button',
            'send_direct_button'
        ]
        for key in self.stats.keys():
            main_elements.append(key)
            main_elements.append(key + '_val()')

        for value in main_elements:
            self.controls[value].Show()
            
    def on_auth(self, event):
        self.toggle_processing(reason='Authorization')
        dialog = FacebookAuthWebView(self, None, -1)
        url = Facebook.get_auth_url()
        dialog.browser.LoadURL(url)
        self.browser = dialog.browser
        dialog.Show()

    def on_settings(self, event):
        dialog = FacebookSettings(self, 'Facebook Bot Settings')
        dialog.Show()

    def on_get_following(self, event):
        if not self.processing:
            url = self.controls['get_following_field'].GetValue()
            if url.find('://facebook.com/') == -1 and url.find('://www.facebook.com/') == -1:
                url = 'https://www.facebook.com/' + url

            if self.following_process is False:
                is_query_url = False
                if url.find('/search/') != -1:
                    self.toggle_processing('Scrapping People By Query')
                    is_query_url = True
                else:
                    self.toggle_processing('Scrapping Friends of "%s"...' % url)

                worker = FollowingThread(self, None, url, None, is_query_url)
                worker.start()
            else:
                is_query_url = False
                if url.find('/search/') != -1:
                    is_query_url = True

                self.toggle_processing('Converting Friends of "%s"...' % url)
                self.following_process = False
                source = self.browser.GetPageSource()
                worker = FollowingThread(self, None, url, source, is_query_url)
                worker.start()

    def on_following_harvested(self, event):
        if event.GetFollowing():
            self.toggle_main()
            self.show_message('Done!')

    def open_following_csv(self, event):
        openFileDialog = wx.FileDialog(self, "Open CSV file", "", "",
                                               "CSV files (*.csv)|*.csv", wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)

        if openFileDialog.ShowModal() == wx.ID_CANCEL:
            return

        ids = []
        try:
            ids = utils.get_csv_content(openFileDialog.GetPath())
        except Exception, e:
            self.show_message('Error! %s' % e.message)

        if len(ids) > 0:
            global FACEBOOK_IDS
            FACEBOOK_IDS = ids

            self.controls['open_following_list'].ClearAll()
            self.controls['open_following_list'].InsertColumn(0, '#')
            self.controls['open_following_list'].InsertColumn(1, 'Facebook ID')
            self.controls['open_following_list'].InsertColumn(2, 'Message')

            for index, value in enumerate(FACEBOOK_IDS):
                pos = self.controls['open_following_list'].InsertStringItem(0, str(index + 1))
                self.controls['open_following_list'].SetStringItem(pos, 1, value)

            self.show_message('Done! Facebook ids loaded')
            return
        self.show_message('Error! %s' % 'No Facebook ids')

    def on_run_bot(self, event):
        if len(FACEBOOK_IDS) > 0:
            global FACEBOOK_BOT_IS_RUNNING
            if FACEBOOK_BOT_IS_RUNNING is True:
                FACEBOOK_BOT_IS_RUNNING = False
                self.controls['bot_run_button'].SetLabel('Run Bot')
            else:

                if FACEBOOK_THREAD_RUNNING is False:
                    FACEBOOK_BOT_IS_RUNNING = True
                    global FACEBOOK_FRIEND_TODAY
                    global FACEBOOK_LAST_REQUESTS
                    FACEBOOK_FRIEND_TODAY = 0
                    FACEBOOK_LAST_REQUESTS = 0
                    self.bot = FacebookBotThread(self)
                    self.bot.start()
                    self.controls['bot_run_button'].SetLabel('Stop Bot')
                    self.update_statistics()
                else:
                    self.show_message('Error! Please, wait until bot thread will end its work.')
            return
        self.show_message('Error! Upload ids to run the bot.')

    def open_messages_csv(self, event):
        if len(FACEBOOK_IDS) > 0:
            openFileDialog = wx.FileDialog(self, "Open CSV file", "", "",
                                                   "CSV files (*.csv)|*.csv", wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)

            if openFileDialog.ShowModal() == wx.ID_CANCEL:
                return

            messages = []
            try:
                messages = utils.get_csv_content(openFileDialog.GetPath())
            except Exception, e:
                self.show_message('Error! %s' % e.message)

            if len(messages) > 0:
                global FACEBOOK_MESSAGES
                FACEBOOK_MESSAGES = messages

                message_index = 0
                for index, value in enumerate(FACEBOOK_IDS):
                    if message_index >= len(FACEBOOK_MESSAGES):
                        message_index = 0

                    try:
                        pos_index = len(FACEBOOK_IDS) - (index+1)
                        self.controls['open_following_list'].SetStringItem(pos_index, 2, FACEBOOK_MESSAGES[message_index])
                    except:
                        break

                    message_index += 1

                self.show_message('Done! Facebook messages loaded')
                return
            self.show_message('Error! %s' % 'No Facebook messages')
        self.show_message('Error! %s' % 'No Facebook Ids')

    def send_messages(self, event):
        if len(FACEBOOK_IDS) > 0 and len(FACEBOOK_MESSAGES) > 0:
            messaging_worker = FacebookMessagingThread(self)
            messaging_worker.start()
            return
        self.show_message('Error! %s' % 'No messages/ids loaded')


class FacebookAuthWebView(wx.Dialog):
    def __init__(self, parent=None, *args, **kwds):
        wx.Dialog.__init__(self, *args, **kwds)
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.browser = wx.html2.WebView.New(self)
        sizer.Add(self.browser, 1, wx.EXPAND, 10)
        self.parent = parent
        self.SetSizer(sizer)
        self.SetSize((700, 700))
        self.Bind(wx.html2.EVT_WEBVIEW_NAVIGATED, self.on_navigated, self.browser)
        self.Bind(EVT_FACEBOOK_AUTH, self.on_authed)
        global Facebook
        self.Bind(wx.html2.EVT_WEBVIEW_LOADED, Facebook.loaded, self.browser)

    def on_navigated(self, event):
        if Facebook.access_token is None:
            targetUrl = event.GetURL()
            worker = AuthThread(self, None, targetUrl)
            worker.start()
        else:
            Facebook.harvest(event)

    def on_authed(self, event):
        global Facebook
        if event.GetToken() and Facebook.access_token is None:
            Facebook = facebook_api.Facebook(access_token=event.GetToken())
            self.parent.toggle_main()
            # TODO: Hide it
            self.Hide()


myEVT_FACEBOOK_AUTH = wx.NewEventType()
EVT_FACEBOOK_AUTH = wx.PyEventBinder(myEVT_FACEBOOK_AUTH, 1)


class AuthEvent(wx.PyCommandEvent):
    def __init__(self, etype, eid, token=None):
        wx.PyCommandEvent.__init__(self, etype, eid)
        self._token = token

    def GetToken(self):
        return self._token


myEVT_FACEBOOK_FOLLOWING = wx.NewEventType()
EVT_FACEBOOK_FOLLOWING = wx.PyEventBinder(myEVT_FACEBOOK_FOLLOWING, 1)


class FollowingEvent(wx.PyCommandEvent):
    def __init__(self, etype, eid, following=None):
        wx.PyCommandEvent.__init__(self, etype, eid)
        self._following = following

    def GetFollowing(self):
        return self._following


class FollowingThread(threading.Thread):
    def __init__(self, parent, following, url, source, is_query):
        threading.Thread.__init__(self)
        self._parent = parent
        self.url = url
        self.source = source
        self._following = following
        self.is_query = is_query

    def run(self):
        if self.url:
            if self.is_query is True:
                if self.source:
                    try:
                        following = Facebook.convert_query_friends_result(self.source)
                        parse = urlparse.urlsplit(self.url)
                        path = parse.path[1:]
                        if path.find('profile.php') != -1:
                            path = self.url.split('profile.php?id=')[1]
                            if path.find('&') != -1:
                               path = self.url.split('&')[0]

                        utils.save_csv('fb_query_ids', following)
                        self._parent.show_message('Done!')
                        self._parent.toggle_main()
                        self._parent.following_process = False
                    except Exception, e:
                        self._parent.show_message('Error! %s' % e.message)
                        self._parent.toggle_main()
                        return
                else:
                    try:
                        following = Facebook.get_query_friends(self.url, self._parent.browser)
                        if following is None:
                            self._parent.following_process = True
                            self._parent.show_message('Browser prepared for harvesting, click one more on the same button, please.')
                            self._parent.toggle_main()
                    except Exception, e:
                        self._parent.show_message('Error! %s' % e.message)
                        self._parent.toggle_main()
                        return
            else:
                if self.source:
                    try:
                        following = Facebook.convert_friends_result(self.source)
                        parse = urlparse.urlsplit(self.url)
                        path = parse.path[1:]
                        if path.find('profile.php') != -1:
                            path = self.url.split('profile.php?id=')[1]
                            if path.find('&') != -1:
                               path = self.url.split('&')[0]

                        utils.save_csv('fb_%s' % path, following)
                        self._parent.show_message('Done!')
                        self._parent.toggle_main()
                        self._parent.following_process = False
                    except Exception, e:
                        self._parent.show_message('Error! %s' % e.message)
                        self._parent.toggle_main()
                        return
                else:
                    try:
                        following = Facebook.get_friends(self.url, self._parent.browser)
                        if following is None:
                            self._parent.following_process = True
                            self._parent.show_message('Browser prepared for harvesting, click one more on the same button, please.')
                            self._parent.toggle_main()
                    except Exception, e:
                        self._parent.show_message('Error! %s' % e.message)
                        self._parent.toggle_main()
                        return

        evt = FollowingEvent(myEVT_FACEBOOK_FOLLOWING, -1, self._following)
        wx.PostEvent(self._parent, evt)


class AuthThread(threading.Thread):
    def __init__(self, parent, token,  url):
        threading.Thread.__init__(self)
        self._parent = parent
        self.url = url
        self._token = token

    def run(self):
        if self.url.find('code=') != -1 and Facebook.access_token is None:
            code = (self.url.split('code=')[1]).split('&')[0]
            self._token = Facebook.get_access_token(code)
            # self._parent.Hide()

        evt = AuthEvent(myEVT_FACEBOOK_AUTH, -1, self._token)
        wx.PostEvent(self._parent, evt)
        
        
class FacebookBotThread(threading.Thread):
    def __init__(self, parent):
        threading.Thread.__init__(self)
        self._parent = parent
        self.waiting_next_day = False
        self._stop = threading.Event()

    def stop(self):
        global FACEBOOK_THREAD_RUNNING
        FACEBOOK_THREAD_RUNNING = False
        self.update('Bot stopped')
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()

    def next_day_came(self):
        global FACEBOOK_FRIEND_TODAY
        global FACEBOOK_LAST_REQUESTS
        FACEBOOK_FRIEND_TODAY = 0
        FACEBOOK_LAST_REQUESTS = 0
        self.waiting_next_day = False

    def send_friend_request(self, user_id):
        global FACEBOOK_LAST_REQUEST_TIME
        global FACEBOOK_FRIEND_TODAY
        if self.count_request():
            FACEBOOK_LAST_REQUEST_TIME = datetime.datetime.now()
            try:
                self.update('Friend for user: %s' % str(user_id))

                Facebook.user_friend(user_id, self._parent.browser)
                FACEBOOK_FRIEND_TODAY += 1
            except:
                pass

        if FACEBOOK_FRIEND_TODAY >= FACEBOOK_FRIEND_PER_DAY:
            self.update('Waiting next day, reached friends per day')
            self.waiting_next_day = True

    def count_request(self):
        global FACEBOOK_LAST_REQUESTS
        if FACEBOOK_LAST_REQUESTS + 1 <= FACEBOOK_LAST_REQUESTS_MAX:
            FACEBOOK_LAST_REQUESTS += 1
            return True

        self.update('Waiting Rate Limit Interval [1 sec]')
        time.sleep(1)

        FACEBOOK_LAST_REQUESTS = 0
        return True

    def update(self, status=None):
        self._parent.update_statistics(status)

    def run(self):
        global FACEBOOK_THREAD_RUNNING
        FACEBOOK_THREAD_RUNNING = True

        index = 0
        global FACEBOOK_BOT_IS_RUNNING
        while FACEBOOK_BOT_IS_RUNNING is True:
            random_secs = random.randrange(1, FACEBOOK_TIME_INTERVAL_MAX + 1, 2)
            self.update('Waiting Random Time [Random: %s secs]' % str(random_secs))
            time.sleep(random_secs)

            if index >= len(FACEBOOK_IDS):
                index = 0

            if self.waiting_next_day is False:
                if self.count_request():
                    self.send_friend_request(FACEBOOK_IDS[index])

                index += 1

            if FACEBOOK_FRIEND_TODAY >= FACEBOOK_FRIEND_PER_DAY:
                self.update('Waiting next day, reached friends per day')
                self.waiting_next_day = True

            global FACEBOOK_TIME
            day = datetime.datetime.now().day
            if day != FACEBOOK_TIME.day:
                self.next_day_came()

            FACEBOOK_TIME = datetime.datetime.now()


        FACEBOOK_THREAD_RUNNING = False
        self.update('Bot stopped')
        return


class FacebookMessagingThread(threading.Thread):
    def __init__(self, parent):
        threading.Thread.__init__(self)
        self._parent = parent
        self._stop = threading.Event()

    def stop(self):
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()

    def send_message(self, user_id, message):
        Facebook.send_message(user_id, message, self._parent.browser)

    def run(self):
        index = 0
        sent = 0
        errors = []

        for user_id in FACEBOOK_IDS:
            if index >= len(FACEBOOK_MESSAGES):
                index = 0
            try:
                self.send_message(user_id, FACEBOOK_MESSAGES[index])
                time.sleep(8)
                sent += 1
            except Exception, e:
                errors.append('Sending to %s, Error message: %s ' % (str(user_id), str(e.message)))
                pass

            index += 1

        self._parent.show_message('Successfuly sent %s / %s messages.' % (str(sent), str(len(FACEBOOK_IDS))))
        if len(errors) > 0:
            self._parent.show_message('Errors: \n\n %s' % '\n'.join(errors))
        return


class FacebookSettings(wx.Frame):
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, title=title, size=(330, 160))
        self.parent = parent
        tilt = 0

        self.FACEBOOK_FRIEND_PER_DAY_label = wx.StaticText(self, label="Friends Per Day:", pos=(10, 10 + tilt))
        self.FACEBOOK_FRIEND_PER_DAY_field = wx.TextCtrl(self, value=str(FACEBOOK_FRIEND_PER_DAY), size=(50, 20), pos=(250, 10 + tilt))
        tilt += 30

        self.FACEBOOK_TIME_INTERVAL_MAX_label = wx.StaticText(self, label="Random Max Time Interval (secs):", pos=(10, 10 + tilt))
        self.FACEBOOK_TIME_INTERVAL_MAX_field = wx.TextCtrl(self, value=str(FACEBOOK_TIME_INTERVAL_MAX), size=(50, 20), pos=(250, 10 + tilt))

        self.save_button = wx.Button(self, label="Save Settings", id=wx.ID_OK, pos=(200, 50 + tilt), size=(100, 30))
        self.save_button.Bind(wx.EVT_BUTTON, self.on_save, id=wx.ID_OK)

        self.Bind(wx.EVT_CLOSE, self.OnExit)

    def on_save(self, event):
        try:
            global FACEBOOK_FRIEND_PER_DAY
            FACEBOOK_FRIEND_PER_DAY = int(self.FACEBOOK_FRIEND_PER_DAY_field.GetValue())

            global FACEBOOK_TIME_INTERVAL_MAX
            FACEBOOK_TIME_INTERVAL_MAX = int(self.FACEBOOK_TIME_INTERVAL_MAX_field.GetValue())
        except Exception, e:
            self.parent.show_message('Error! %s' % e.message)
        self.parent.update_statistics()
        self.Destroy()

    def OnExit(self, e):
        self.Destroy()