# coding=utf-8
import os
import wx
import wx.html2
import socials.api_twitter as twitter_api
import socials.api_instagram as instagram_api
import socials.api_facebook as facebook_api
import utils
from logger import log

os.environ['REQUESTS_CA_BUNDLE'] = os.path.join(os.path.join(utils.BASE_DIR, 'ssl'), 'cacert.pem')

Twitter = twitter_api.Twitter()
Instagram = instagram_api.Instagram()
Facebook = facebook_api.Facebook()


from ui_twitter import TwitterPage
from ui_instagram import InstagramPage
from ui_facebook import FacebookPage


class MainWindow(wx.Frame):
    def __init__(self, parent, title, app=None):
        wx.Frame.__init__(self, parent, title=title, size=(600, 800))

        self.app = app

        p = wx.Panel(self)
        nb = wx.Notebook(p)

        twitter_page = TwitterPage(nb)
        instagram_page = InstagramPage(nb)
        facebook_page = FacebookPage(nb)

        nb.AddPage(twitter_page, "Twitter Page")
        nb.AddPage(instagram_page, "Instagram Page")
        nb.AddPage(facebook_page, "Facebook Page")

        sizer = wx.BoxSizer()
        sizer.Add(nb, 1, wx.EXPAND)
        p.SetSizer(sizer)

        self.Show(True)

        menu = wx.Menu()
        aboutItem = menu.Append(wx.ID_ABOUT, "About", "Push the button to get an information about this application")
        exitItem = menu.Append(wx.ID_EXIT, "Exit", "Push the button to leave this application")
        bar = wx.MenuBar()
        bar.Append(menu,"File")
        self.SetMenuBar(bar)
        self.Bind(wx.EVT_MENU, self.OnAbout, aboutItem)
        self.Bind(wx.EVT_MENU, self.OnExit, exitItem)
        self.Bind(wx.EVT_CLOSE, self.OnExit)

    def OnAbout(self, e):
        aboutDlg = wx.MessageDialog(self, "Social-Scrapper-Bot\nhttps://bitbucket.org/Dialord/social-media-scrapping-bot\nGPL LICENSED\nAuthor: Yegor Dia\nEmail: yegordia@gmail.com", "About", wx.OK)
        aboutDlg.ShowModal()

    def OnExit(self, e):
        self.app.Exit()


class TwitterWindow(wx.Frame):
    def __init__(self, parent, title, app=None):
        wx.Frame.__init__(self, parent, title=title, size=(600, 800))

        self.app = app

        p = wx.Panel(self)
        box = wx.BoxSizer(wx.VERTICAL)

        twitter_page = TwitterPage(p)
        twitter_page.SetSize(size=(600, 800))

        box.Add(twitter_page, 0, wx.ALL, 10)

        self.Show(True)

        menu = wx.Menu()
        aboutItem = menu.Append(wx.ID_ABOUT, "About", "Push the button to get an information about this application")
        exitItem = menu.Append(wx.ID_EXIT, "Exit", "Push the button to leave this application")
        bar = wx.MenuBar()
        bar.Append(menu,"File")
        self.SetMenuBar(bar)
        self.Bind(wx.EVT_MENU, self.OnAbout, aboutItem)
        self.Bind(wx.EVT_MENU, self.OnExit, exitItem)
        self.Bind(wx.EVT_CLOSE, self.OnExit)

    def OnAbout(self, e):
        aboutDlg = wx.MessageDialog(self, "Social-Scrapper-Bot For Twitter\nhttps://bitbucket.org/Dialord/social-media-scrapping-bot\nGPL LICENSED\nAuthor: Yegor Dia\nEmail: yegordia@gmail.com", "About", wx.OK)
        aboutDlg.ShowModal()

    def OnExit(self, e):
        self.app.Exit()


class InstagramWindow(wx.Frame):
    def __init__(self, parent, title, app=None):
        wx.Frame.__init__(self, parent, title=title, size=(600, 800))

        self.app = app

        p = wx.Panel(self)
        box = wx.BoxSizer(wx.VERTICAL)

        instagram_page = InstagramPage(p)
        instagram_page.SetSize(size=(600, 800))

        box.Add(instagram_page, 0, wx.ALL, 10)

        self.Show(True)

        menu = wx.Menu()
        aboutItem = menu.Append(wx.ID_ABOUT, "About", "Push the button to get an information about this application")
        exitItem = menu.Append(wx.ID_EXIT, "Exit", "Push the button to leave this application")
        bar = wx.MenuBar()
        bar.Append(menu, "File")
        self.SetMenuBar(bar)
        self.Bind(wx.EVT_MENU, self.OnAbout, aboutItem)
        self.Bind(wx.EVT_MENU, self.OnExit, exitItem)
        self.Bind(wx.EVT_CLOSE, self.OnExit)

    def OnAbout(self, e):
        aboutDlg = wx.MessageDialog(self, "Social-Scrapper-Bot For Instagram\nhttps://bitbucket.org/Dialord/social-media-scrapping-bot\nGPL LICENSED\nAuthor: Yegor Dia\nEmail: yegordia@gmail.com", "About", wx.OK)
        aboutDlg.ShowModal()

    def OnExit(self, e):
        self.app.Exit()


class FacebookWindow(wx.Frame):
    def __init__(self, parent, title, app=None):
        wx.Frame.__init__(self, parent, title=title, size=(600, 800))

        self.app = app

        p = wx.Panel(self)
        box = wx.BoxSizer(wx.VERTICAL)

        facebook_page = FacebookPage(p)
        facebook_page.SetSize(size=(600, 800))

        box.Add(facebook_page, 0, wx.ALL, 10)

        self.Show(True)

        menu = wx.Menu()
        aboutItem = menu.Append(wx.ID_ABOUT, "About", "Push the button to get an information about this application")
        exitItem = menu.Append(wx.ID_EXIT, "Exit", "Push the button to leave this application")
        bar = wx.MenuBar()
        bar.Append(menu, "File")
        self.SetMenuBar(bar)
        self.Bind(wx.EVT_MENU, self.OnAbout, aboutItem)
        self.Bind(wx.EVT_MENU, self.OnExit, exitItem)
        self.Bind(wx.EVT_CLOSE, self.OnExit)

    def OnAbout(self, e):
        aboutDlg = wx.MessageDialog(self, "Social-Scrapper-Bot For Facebook\nhttps://bitbucket.org/Dialord/social-media-scrapping-bot\nGPL LICENSED\nAuthor: Yegor Dia\nEmail: yegordia@gmail.com", "About", wx.OK)
        aboutDlg.ShowModal()

    def OnExit(self, e):
        self.app.Exit()