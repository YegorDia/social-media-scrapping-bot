import logging

logging.basicConfig(filename='debug-log.log', level=logging.DEBUG)


def log(msg, *args):
    logging.debug(msg, *args)
