import requests
from logger import log

client_id = "aa49dd7cdbc343d6830137b009fe9226"
client_secret = "73abd57d04c74d82a0a9d434810e2d8f"
redirect_uri = 'https://instagram.com/'

ACTION = None
HARVESTING = False
RESULT = []
HELP = {}
CHECKERTHREAD_CALL = False


class Instagram(object):
    def __init__(self, access_token=None):
        self.instagram_user = None
        if access_token:
            self.access_token = access_token
            self.api = 'Custom'
        else:
            self.access_token = None
            self.api = None

    def get_auth_url(self):
        return 'https://api.instagram.com/oauth/authorize?scope=likes comments relationships&redirect_uri=%s&response_type=code&client_id=%s' % (
            redirect_uri, client_id)

    def get_access_token(self, code):

        try:
            access_token_request = requests.post('https://api.instagram.com/oauth/access_token', data={
                'client_id': client_id,
                'client_secret': client_secret,
                'grant_type': 'authorization_code',
                'redirect_uri': redirect_uri,
                'code': code
            })

            response = access_token_request.json()
            self.access_token = response['access_token']
            self.api = 'Custom'
            self.instagram_user = response['user']
        except:
            pass

        return self.access_token

    def process_username(self, username):
        response = requests.get(
            'https://api.instagram.com/v1/users/search?q=%s&access_token=%s' % (
                username, self.access_token)).json()
        for user in response["data"]:
            return user['id']

    def get_following(self, username):
        user_id = self.process_username(username)

        data = []
        url = 'https://api.instagram.com/v1/users/%s/followed-by?access_token=%s&count=100' % (user_id, self.access_token)
        while True:
            response = requests.get(url).json()
            data.extend(response['data'])
            if 'pagination' in response and 'next_url' in response['pagination']:
                url = response['pagination']['next_url']
            else:
                break
        return data

    def get_posts(self, user_id, count=20):

        data = []
        url = 'https://api.instagram.com/v1/users/%s/media/recent?access_token=%s&count=%s' % (user_id, self.access_token, str(count))
        response = requests.get(url).json()
        try:
            data.extend(response['data'])

            if len(data) > count:
                data = data[count:]
        except:
            pass

        return data

    def post_like(self, link, browser):
        # url = 'https://api.instagram.com/v1/media/%s/likes' % str(id)
        # response = requests.post(url, data={'access_token':  self.access_token}).json()
        #
        # return response

        global HARVESTING
        global ACTION
        HARVESTING = True
        ACTION = 'send_like'
        browser.LoadURL(link)

    def post_comment(self, link, message, browser):
        # url = 'https://api.instagram.com/v1/media/%s/comments' % str(id)
        # response = requests.post(url, data={'access_token':  self.access_token, 'text': text}).json()
        #
        # return response

        global HARVESTING
        global ACTION
        global HELP
        HARVESTING = True
        HELP['message'] = message
        ACTION = 'send_comment'
        browser.LoadURL(link)

    def loaded(self, event):
        global HARVESTING
        global ACTION
        global HELP

        if HARVESTING is True:
            browser = event.EventObject
            source = browser.GetPageSource()

            if ACTION == 'send_like':
                browser.RunScript("document.getElementsByClassName('ibInner')[0].click();")
                browser.RunScript("document.getElementsByClassName('like-button')[0].click();")

                ACTION = None
                HARVESTING = False
                return

            elif ACTION == 'send_comment':
                message = HELP['message']
                browser.RunScript("var _intervalio = setInterval(function(){document.getElementsByTagName('textarea')[0].value = '%s';},10); setTimeout(function(){ clearInterval(_intervalio); document.getElementById('comment_submit').click();},50);" % message)
                ACTION = None
                HARVESTING = False
                return