import requests
import facebook
import utils
import time
import threading
import datetime
import urlparse
from lxml import etree
from logger import log


FACEBOOK_APP_ID = "326494164215954"
FACEBOOK_APP_SECRET = "57d7a222dc8daba88a6b02d81cfa5072"
redirect_uri = 'http://facebook.com/'

ACTION = None
HARVESTING = False
RESULT = []
HELP = {}
CHECKERTHREAD_CALL = False


def find_between(s, first, last):
    try:
        start = s.find(first) + len(first)
        _s = s[start:]
        end = _s.find(last)
        return _s[:end]
    except ValueError:
        return ""


class Facebook(object):
    def __init__(self, access_token=None):
        if access_token:
            self.access_token = access_token
            self.api = facebook.GraphAPI(access_token=self.access_token, version="2.0")
        else:
            self.access_token = None
            self.api = None

    def process_url(self, url):
        if url.find('facebook.com/') != -1:
            params = utils.get_url_params(url)
            if 'id' in params:
                return {'url': url, 'id': params['id'], 'type': 'profile'}
            return {'url': url, 'id': params['fbid'], 'type': 'object'}

        return {'url': url, 'type': 'error'}

    def get_access_token(self, code):
        response = requests.get('https://graph.facebook.com/oauth/access_token', params={'redirect_uri': redirect_uri,
                                                                                         'client_id': FACEBOOK_APP_ID,
                                                                                         'client_secret': FACEBOOK_APP_SECRET,
                                                                                         'code': code})
        data = response.text
        return (data.split('access_token=')[1]).split('&')[0]

    def get_auth_url(self):
        return 'https://graph.facebook.com/oauth/authorize?scope=%s&redirect_uri=%s&client_id=%s' % ('user_friends', redirect_uri, FACEBOOK_APP_ID)

    def get_friends(self, url, browser):
        if url.find('facebook.com'):
            global HARVESTING
            global ACTION
            global RESULT
            global CHECKERTHREAD_CALL

            parsed_url = urlparse.urlsplit(url)
            params = utils.get_url_params(url)

            RESULT = []
            HARVESTING = True
            ACTION = 'get_friends'

            if parsed_url.path.find('profile.php') != -1:
                browser.LoadURL(parsed_url.geturl() + '?id=' + params['id'])
            else:
                browser.LoadURL(parsed_url.geturl())

            while HARVESTING is True:
                time.sleep(5)
                if CHECKERTHREAD_CALL is True:
                    break

            return None
        raise Exception("Required facebook url!")

    def convert_friends_result(self, source):
        tree = etree.HTML(source)
        friend_trees = tree.xpath(".//*[@class='_698']")
        result = []
        for elem in friend_trees:
            try:
                link = elem.xpath('.//a')[0]
                url = link.attrib["href"]
                if url.find('profile.php') != -1:
                    url = url.split('&')[0].split('.com/')[1]
                    url = url.split('?id=')[1]
                else:
                    url = url.split('?')[0].split('.com/')[1]
                    response = requests.get('https://graph.facebook.com/%s' % url).json()
                    url = str(response['id'])

                result.append(url)
            except:
                pass

        return result


    def get_query_friends(self, url, browser):
        if url.find('facebook.com'):
            global HARVESTING
            global ACTION
            global RESULT
            global CHECKERTHREAD_CALL

            RESULT = []
            HARVESTING = True
            ACTION = 'get_query_friends'

            browser.LoadURL(url)

            while HARVESTING is True:
                time.sleep(5)
                if CHECKERTHREAD_CALL is True:
                    break

            return None
        raise Exception("Required facebook url!")

    def convert_query_friends_result(self, source):
        tree = etree.HTML(source)
        friend_trees = tree.xpath(".//*[@id='initial_browse_result']")[0]
        links = friend_trees.xpath('.//a')
        result = []
        skip = 3
        for link in links:
            if skip > 0:
                skip -= 1
            else:
                try:
                    url = link.attrib["href"]
                    if url.find('profile.php') != -1:
                        url = url.split('&')[0].split('.com/')[1]
                        url = url.split('?id=')[1]
                    else:
                        url = url.split('?')[0].split('.com/')[1]
                        response = requests.get('https://graph.facebook.com/%s' % url).json()
                        url = str(response['id'])

                    result.append(url)
                except:
                    pass

        return result

    def send_message(self, user_id, message, browser):
        global HARVESTING
        global ACTION
        global HELP
        HARVESTING = True
        ACTION = 'send_message'
        HELP['message'] = message
        browser.LoadURL('https://www.facebook.com/messages/%s' % str(user_id))

    def user_friend(self, user_id, browser):
        global HARVESTING
        global ACTION
        HARVESTING = True
        ACTION = 'send_friend_request'
        browser.LoadURL('https://www.facebook.com/profile.php?id=%s' % str(user_id))

    # WebView event OnNavigate
    def harvest(self, event):
        global HARVESTING
        global ACTION

        if HARVESTING is True:
            browser = event.EventObject
            # source = event.EventObject.GetPageSource()
            if ACTION == 'send_friend_request':

                browser.RunScript("document.getElementsByClassName('FriendRequestAdd')[0].click();")
                browser.RunScript("document.getElementsByClassName('FriendRequestAdd')[1].click();")
                browser.RunScript("document.getElementsByClassName('FriendRequestAdd')[2].click();")
                browser.RunScript("document.getElementsByClassName('addButton')[0].click();")
                browser.RunScript("document.getElementsByClassName('addButton')[1].click();")
                browser.RunScript("document.getElementsByClassName('addButton')[2].click();")
                return
            elif ACTION == 'send_message':
                message = HELP['message']
                # browser.RunScript("setInterval(function(){document.getElementsByTagName('textarea')[0].value = '%s';},10);" % message)
                return

    # WebView event OnLoad
    def loaded(self, event):
        global HARVESTING
        global ACTION
        global HELP

        if HARVESTING is True:
            browser = event.EventObject
            source = browser.GetPageSource()

            if ACTION == 'get_friends':
                if source.find('Friends') != -1:
                    url = event.GetURL()
                    ACTION = 'get_friends_FriendsPage'
                    HARVESTING = True

                    try:
                        friends = int(find_between(source, '/friends_mutual">', '</a>').replace(',', ''))
                        HELP['friends_count'] = friends
                    except Exception, e:
                        HELP['friends_count'] = None
                        pass

                    if url.find('profile.php') != -1:
                        browser.LoadURL(url + "%2Ffriends_all&sk=friends")
                    else:
                        if url.find('?') != -1:
                            url = url.split('?')[0]
                        browser.LoadURL(url + "/friends_all")
                    return

                raise Exception('No Friends on get_friends')

            elif ACTION == 'get_query_friends':
                time_process = 100
                ACTION = 'get_query_friends_FriendsPageHarvested'
                HARVESTING = True
                browser.RunScript('var count = %s; var interval = setInterval(function(){ if (count > 0) { x = 0; y = document.body.scrollHeight; window.scroll(x,y); count--;} else {clearInterval(interval);} }, 500);' % str(time_process))
                worker = CheckerThread(time_process*0.5)
                worker.start()

                return

            elif ACTION == 'get_friends_FriendsPage':
                time_process = 100
                if HELP['friends_count'] is not None:
                    time_process = int(HELP['friends_count'] / 10)

                ACTION = 'get_friends_FriendsPageHarvested'
                HARVESTING = True
                browser.RunScript('var count = %s; var interval = setInterval(function(){ if (count > 0) { x = 0; y = document.body.scrollHeight; window.scroll(x,y); count--;} else {clearInterval(interval);} }, 500);' % str(time_process))
                worker = CheckerThread(time_process*0.5)
                worker.start()

                return

            elif ACTION == 'send_friend_request':
                browser.RunScript("document.getElementsByClassName('FriendRequestAdd')[0].click();")
                browser.RunScript("document.getElementsByClassName('FriendRequestAdd')[1].click();")
                browser.RunScript("document.getElementsByClassName('FriendRequestAdd')[2].click();")
                browser.RunScript("document.getElementsByClassName('addButton')[0].click();")
                browser.RunScript("document.getElementsByClassName('addButton')[1].click();")
                browser.RunScript("document.getElementsByClassName('addButton')[2].click();")
                ACTION = None
                HARVESTING = False
                return

            elif ACTION == 'send_message':
                message = HELP['message']
                browser.RunScript("var _intervalio = setInterval(function(){document.getElementsByTagName('textarea')[0].value = '%s';},10); setTimeout(function(){ clearInterval(_intervalio); document.getElementsByClassName('uiButtonConfirm')[0].click();},50);" % message)
                ACTION = None
                HARVESTING = False
                return


class CheckerThread(threading.Thread):
    def __init__(self, wait):
        threading.Thread.__init__(self)
        self.waiting_time = wait
        self.start_time = datetime.datetime.now()

    def run(self):
        global CHECKERTHREAD_CALL
        CHECKERTHREAD_CALL = False
        while True:
            end_time = datetime.datetime.now()
            if (end_time-self.start_time).seconds >= self.waiting_time:
                CHECKERTHREAD_CALL = True
                return