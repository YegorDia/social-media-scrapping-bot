import twitter
import requests_oauthlib
from logger import log

consumer_key = "FqO58VsFNl5vGV1mcW58W8VDB"
consumer_secret = "86y6BbLTp1KA9ymjx51auHW6ccpBdYfbqMn8It07VzGrW7HUiB"

REQUEST_TOKEN_URL = 'https://api.twitter.com/oauth/request_token'
ACCESS_TOKEN_URL = 'https://api.twitter.com/oauth/access_token'
AUTHORIZATION_URL = 'https://api.twitter.com/oauth/authorize'
SIGNIN_URL = 'https://api.twitter.com/oauth/authenticate'


class Twitter(object):
    def __init__(self, access_token=None, access_token_secret=None):
        self.access_token = access_token
        self.access_token_secret = access_token_secret

        if access_token and access_token_secret:
            self.api = twitter.Api(consumer_key=consumer_key,
                                   consumer_secret=consumer_secret,
                                   access_token_key=access_token,
                                   access_token_secret=access_token_secret)
            self.auth = None
        else:
            self.oauth_client = requests_oauthlib.OAuth1Session(consumer_key, client_secret=consumer_secret)
            self.request_token_response = None
            self.api = None
            self.auth = None

    def get_auth_url(self):
        try:
            self.request_token_response = self.oauth_client.fetch_request_token(REQUEST_TOKEN_URL)
        except ValueError, e:
            print 'Invalid respond from Twitter requesting temp token: %s' % e
            return

        return self.oauth_client.authorization_url(AUTHORIZATION_URL)

    def get_access_token(self, verifier):
        oauth_client = requests_oauthlib.OAuth1Session(consumer_key, client_secret=consumer_secret,
                                     resource_owner_key=self.request_token_response.get('oauth_token'),
                                     resource_owner_secret=self.request_token_response.get('oauth_token_secret'),
                                     verifier=verifier
        )
        try:
            resp = oauth_client.fetch_access_token(ACCESS_TOKEN_URL)
        except ValueError, e:
            print 'Invalid respond from Twitter requesting access token: %s' % e
            return

        self.access_token = resp.get('oauth_token')
        self.access_token_secret = resp.get('oauth_token_secret')

        self.api = twitter.Api(consumer_key=consumer_key,
                                   consumer_secret=consumer_secret,
                                   access_token_key=self.access_token,
                                   access_token_secret=self.access_token_secret)

        self.auth = requests_oauthlib.OAuth1(consumer_key, consumer_secret, self.access_token, self.access_token_secret)

        self.api.__auth = self.auth

        return {
            'access_token': self.access_token,
            'access_token_secret': self.access_token_secret
        }

    def get_following(self, username):
        return self.api.GetFollowers(screen_name=username, count=200)

    def get_posts(self, username, count=20):
        return self.api.GetUserTimeline(screen_name=username, count=count)

    def post_like(self, id):
        return self.api.CreateFavorite(id=id)

    def send_direct_message(self, username, message):
        self.api.PostDirectMessage(screen_name=username, text=message)


