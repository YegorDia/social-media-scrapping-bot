import csv
import os
import sys
import xlrd


def get_url_params(url):
    if len(url) > 0:
        params = {}
        if url[-1] != '/':
            non_domain_part = url.split('/')[-1]

            if non_domain_part.find('?') != -1:
                params_part = non_domain_part.split('?')[-1]
                if params_part.find('&') != -1:
                    params_str = params_part.split('&')
                    for p in params_str:
                        temp = p.split('=')
                        if len(temp) > 1:
                            params[temp[0]] = temp[1]
                        else:
                            params[temp[0]] = None
                    return params

                param = params_part.split('=')
                params[param[0]] = param[1]
        return params
    raise Exception("Invalid URL")


def get_script_directory():
    path = os.path.realpath(sys.argv[0])
    if os.path.isdir(path):
        return path
    else:
        return os.path.dirname(path)

BASE_DIR = get_script_directory()


def get_csv_content(filename):
    ids = []
    try:
        rb = xlrd.open_workbook(filename, formatting_info=True)
        sheet = rb.sheet_by_index(0)
        for rownum in range(sheet.nrows):
            row = sheet.row_values(rownum)

            for cell in row:
                ids.append(cell)
        return ids
    except:
        with open(filename, 'rb') as csvfile:
            csv_reader = csv.reader(csvfile, delimiter=';', quotechar='|')
            for row in csv_reader:
                ids.append(row[0])
    return ids


def save_csv(file_name, content):
    try:
        path = os.path.join(BASE_DIR, ('%s.csv' % file_name))
        with open(path, 'wb') as csvfile:
            csv_writer = csv.writer(csvfile, delimiter=';',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)

            for index, val in enumerate(content):
                csv_writer.writerow([val])
        return True
    except:
        pass
    return False