from lxml import _elementpath as _dummy
from distutils.core import setup
import py2exe



# setup(
#     options={'py2exe': {'bundle_files': 1, 'dll_excludes': 'w9xpopen.exe', 'compressed': True, 'includes': ['lxml.etree', 'lxml._elementpath', 'gzip']}},
#     windows=[
#         {
#             'script': "main.py",
#             "icon_resources": [(1, "icon.ico")],
#             "dest_base": "social-scrapper-bot",
#             "copyright": "Copyright (C) 2015 UrbanGreenHouse"
#         }
#     ],
#     zipfile=None, requires=['requests', 'twitter',  'xlwt', 'logging', 'xlrd', 'requests_oauthlib', 'lxml'],
#     version="1.0.0.0",
#     author="Yegor Dia",
#     author_email="yegordia@gmail.com",
#     name="Social Scrapper Bot By Yegor Dia [yegordia@gmail.com]",
#     description="Bot created for harvesting information deep from social medias like: tw, fb, insta and send messages/comments etc."
# )

# twitter build
setup(
    options={'py2exe': {'bundle_files': 1, 'dll_excludes': 'w9xpopen.exe', 'compressed': True, 'includes': ['lxml.etree', 'lxml._elementpath', 'gzip']}},
    windows=[
        {
            'script': "main_twitter.py",
            "icon_resources": [(1, "icon_tw.ico")],
            "dest_base": "twitter-scrapper-bot",
            "copyright": "Copyright (C) 2015 UrbanGreenHouse"
        }
    ],
    zipfile=None, requires=['requests', 'twitter',  'xlwt', 'logging', 'xlrd', 'requests_oauthlib', 'lxml'],
    version="1.0.0.0",
    author="Yegor Dia",
    author_email="yegordia@gmail.com",
    name="Social Twitter Scrapper Bot By Yegor Dia [yegordia@gmail.com]",
    description="Bot created for harvesting information deep from social media (Twitter)"
)


# facebook build
setup(
    options={'py2exe': {'bundle_files': 1, 'dll_excludes': 'w9xpopen.exe', 'compressed': True, 'includes': ['lxml.etree', 'lxml._elementpath', 'gzip']}},
    windows=[
        {
            'script': "main_facebook.py",
            "icon_resources": [(1, "icon_fb.ico")],
            "dest_base": "facebook-scrapper-bot",
            "copyright": "Copyright (C) 2015 UrbanGreenHouse"
        }
    ],
    zipfile=None, requires=['requests', 'twitter',  'xlwt', 'logging', 'xlrd', 'requests_oauthlib', 'lxml'],
    version="1.0.0.0",
    author="Yegor Dia",
    author_email="yegordia@gmail.com",
    name="Social Twitter Scrapper Bot By Yegor Dia [yegordia@gmail.com]",
    description="Bot created for harvesting information deep from social media (Facebook)"
)


# instagram build
setup(
    options={'py2exe': {'bundle_files': 1, 'dll_excludes': 'w9xpopen.exe', 'compressed': True, 'includes': ['lxml.etree', 'lxml._elementpath', 'gzip']}},
    windows=[
        {
            'script': "main_instagram.py",
            "icon_resources": [(1, "icon_in.ico")],
            "dest_base": "instagram-scrapper-bot",
            "copyright": "Copyright (C) 2015 UrbanGreenHouse"
        }
    ],
    zipfile=None, requires=['requests', 'twitter',  'xlwt', 'logging', 'xlrd', 'requests_oauthlib', 'lxml'],
    version="1.0.0.0",
    author="Yegor Dia",
    author_email="yegordia@gmail.com",
    name="Social Twitter Scrapper Bot By Yegor Dia [yegordia@gmail.com]",
    description="Bot created for harvesting information deep from social media (Instagram)"
)