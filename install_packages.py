import pip

packages = ['python-twitter', 'csv', 'xlwt', 'logging', 'xlrd', 'requests_oauthlib']


def install(package):
    pip.main(["install", "--pre", "--upgrade", "--no-index",
            "--find-links=.", package, "--log-file", "log.txt", "-vv"])
    pip.main(["install", package])


def install_packages():
    for package in packages:
        install(package)

if __name__ == "__main__":
    install_packages()